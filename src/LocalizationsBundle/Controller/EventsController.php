<?php

namespace LocalizationsBundle\Controller;

use LocalizationsBundle\Entity\Event;
use LocalizationsBundle\Entity\EventComment;
use LocalizationsBundle\Form\Type\CommentType;
use LocalizationsBundle\Form\Type\EventType;

use LocalizationsBundle\Form\Type\FilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Default controller for events.
 *
 * @package    LocalizationsBundle
 * @subpackage DependencyInjection
 */
class EventsController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        # create filtering form
        $filterForm = $this->get('form.factory')->create(FilterType::class);
        $filterForm->handleRequest($request);

        # get coordinates for particular address
        $coords = $this->get('geocode')->getCoordinates(
            $filterForm->getData()['address']
        );

        # if coords are not empty, find events
        if($coords !== []) {
            $events = $this->getDoctrine()->getRepository('LocalizationsBundle:Event')
                ->eventsInRadius($coords['lat'], $coords['long'], 2);
        } # else, get all events
        else {
            # load all events
            $events = $this->getDoctrine()
                ->getRepository('LocalizationsBundle:Event')
                ->getEvents();
        }

        # render view
        return $this->render('LocalizationsBundle::list.html.twig', [
            'events'     => $events,
            'title'      => 'Events list',
            'filterForm' => $filterForm->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function profileAction(Request $request)
    {
        # get param
        $id = $request->get('id');

        # load parrticular event
        $event = $this->getDoctrine()
            ->getRepository('LocalizationsBundle:Event')
            ->getEvent($id);

        # create form used for adding comments
        $form = $this->get('form.factory')->create(CommentType::class);
        $form->handleRequest($request);

        # if form was submitted and is valid, add comment
        if($form->isSubmitted() && $form->isValid()) {
            /* @var $comment EventComment */
            $comment = $form->getData();
            $comment->setEvent($event);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $this->addFlash('notice', 'Comment added!');

            return $this->redirectToRoute('event_profile', [
                'id' => $id,
            ]);
        }

        # render view
        return $this->render('LocalizationsBundle::profile.html.twig', [
            'event'    => $event,
            'title'    => $event->getName(),
            'form'     => $form->createView(),
            'comments' => $this->getDoctrine()
                ->getRepository('LocalizationsBundle:EventComment')
                ->getForEvent(
                    $event->getId()
                ),
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        # page title
        $title = 'New event';

        # create new Event instance
        $event = new Event();

        # create form
        $form = $this->get('form.factory')->create(EventType::class, $event, []);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();

            $this->addFlash('notice', 'Event added!');

            return $this->redirectToRoute('events_create');
        }

        # render view
        return $this->render('LocalizationsBundle::form_view.html.twig', [
            'title' => $title,
            'form'  => $form->createView(),
        ]);
    }
}
