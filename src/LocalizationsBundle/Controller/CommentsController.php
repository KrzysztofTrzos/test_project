<?php

namespace LocalizationsBundle\Controller;

use LocalizationsBundle\Entity\EventComment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for events comments.
 *
 * @package    LocalizationsBundle
 * @subpackage Controller
 */
class CommentsController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function deleteCommentAction(Request $request)
    {
        # get param
        $id = $request->get('id');

        # load comment
        $comment = $this->getDoctrine()->getManager()
            ->find(EventComment::class, $id);

        # check if comment exists
        if($comment instanceof EventComment) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comment);
            $em->flush();

            $this->addFlash('notice', 'Comment deleted!');

            return $this->redirectToRoute('event_profile', [
                'id' => $comment->getEvent()->getId(),
            ]);
        }

        # return to events list if comment does not exist
        return $this->redirectToRoute('events');
    }
}
