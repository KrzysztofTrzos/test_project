<?php
namespace LocalizationsBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use LocalizationsBundle\Entity\Event;
use Swift_Message;
use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * This class is used to listen on events related with Doctrine.
 *
 * @package    LocalizationsBundle
 * @subpackage EventListener
 */
class EventsEntitiesSubscriber implements EventSubscriber
{
    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * EventsEntitiesSubscriber constructor.
     *
     * @param TwigEngine    $twig
     * @param \Swift_Mailer $mailer
     */
    public function __construct(TwigEngine $twig, \Swift_Mailer $mailer)
    {
        $this->twig   = $twig;
        $this->mailer = $mailer;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        /* @var $obj Event */
        $obj = $args->getObject();

        $email     = 'admin@domain.com';
        $emailName = 'Admin';

        /* @var $message Swift_Message */
        $message = Swift_Message::newInstance()
            ->setSubject('Message from contact section')
            ->setFrom(
                [
                    $email => $emailName,
                ],
                $emailName
            )
            ->setTo($email)
            ->setBody(
                $this->twig->render('@Localizations/email/event_created.html.twig', [
                    'event' => $obj,
                ]),
                'text/html'
            );

        $this->mailer->send($message);
    }
}