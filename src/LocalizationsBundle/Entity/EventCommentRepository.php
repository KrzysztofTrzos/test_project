<?php

namespace LocalizationsBundle\Entity;

use Doctrine\ORM;

/**
 * Repository class for Event entity.
 *
 * @package    LocalizationsBundle
 * @subpackage Entity
 */
class EventCommentRepository extends ORM\EntityRepository
{
    /**
     * @param int $eventId
     * @return EventComment[]
     */
    public function getForEvent($eventId)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->orderBy('c.creationDate', 'DESC');

        return $qb->getQuery()->execute();
    }
}