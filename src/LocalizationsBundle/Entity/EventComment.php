<?php

namespace LocalizationsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use LocalizationsBundle\Custom\LocalizationsModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Event
 *
 * @package    LocalizationsBundle
 * @subpackage Entity
 *
 * @ORM\Table(name="events_comments")
 * @ORM\Entity(repositoryClass="EventCommentRepository")
 * @ORM\HasLifecycleCallbacks
 */
class EventComment extends LocalizationsModel
{
    /**
     * Event ID.
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=254, nullable=false)
     * @Assert\Email()
     * @Assert\NotBlank()
     * @Assert\Length(max="254")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=500, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="500")
     */
    private $content;

    /**
     * @var Event
     *
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="comments")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id", nullable=false)
     */
    protected $event;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $creationDate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->creationDate = new \DateTime();
    }

    /**
     * Get event ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     */
    public function setEvent(Event $event)
    {
        $this->event = $event;
    }
}