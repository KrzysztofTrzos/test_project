<?php

namespace LocalizationsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use LocalizationsBundle\Custom\LocalizationsModel;
use Symfony\Component\Validator\Constraints as Assert;

use LocalizationsBundle\Validator\Constraints as LocalAssert;

/**
 * Class Event
 *
 * @package    LocalizationsBundle
 * @subpackage Entity
 *
 * @ORM\Table(name="events")
 * @ORM\Entity(repositoryClass="EventRepository")
 */
class Event extends LocalizationsModel
{
    /**
     * Event ID.
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=300, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="300")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=50, nullable=false)
     * @Assert\NotBlank()
     * @LocalAssert\AddressGeocode()
     * @Assert\Length(max="50")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=254, nullable=false)
     * @Assert\Email()
     * @Assert\NotBlank()
     * @Assert\Length(max="254")
     */
    private $email;

    /**
     * @ORM\OneToMany(
     *     targetEntity="EventComment",
     *     mappedBy="event",
     *     cascade={"persist","remove"},
     *     orphanRemoval=true
     * )
     * @var EventComment[]
     */
    protected $comments;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_from", type="date", nullable=false)
     * @Assert\NotBlank()
     */
    private $dateFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_to", type="date", nullable=false)
     * @Assert\NotBlank()
     */
    private $dateTo;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=false)
     */
    private $latitude;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=false)
     */
    private $longitude;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    /**
     * Get event ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * @param \DateTime|array $dateFrom
     */
    public function setDateFrom($dateFrom)
    {
        $value = $dateFrom;

        if(
            is_array($dateFrom)
            && array_key_exists('year', $dateFrom)
            && array_key_exists('month', $dateFrom)
            && array_key_exists('day', $dateFrom)
        ) {
            $value = new \DateTime($dateFrom['year'].'-'.$dateFrom['month'].'-'.$dateFrom['day']);
        }

        $this->dateFrom = $value;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * @param \DateTime|array $dateTo
     */
    public function setDateTo($dateTo)
    {
        $value = $dateTo;

        if(
            is_array($dateTo)
            && array_key_exists('year', $dateTo)
            && array_key_exists('month', $dateTo)
            && array_key_exists('day', $dateTo)
        ) {
            $value = new \DateTime($dateTo['year'].'-'.$dateTo['month'].'-'.$dateTo['day']);
        }

        $this->dateTo = $value;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }


    /**
     * @return EventComment[]|ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param EventComment[] $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }
}