<?php

namespace LocalizationsBundle\Entity;

use Doctrine\ORM;
use Doctrine\ORM\NoResultException;

/**
 * Repository class for Event entity.
 *
 * @package    LocalizationsBundle
 * @subpackage Entity
 */
class EventRepository extends ORM\EntityRepository
{
    /**
     * @return Event[]
     */
    public function getEvents()
    {
        $qb = $this->createQueryBuilder('b');
        $qb->orderBy('b.dateFrom', 'DESC');

        return $qb->getQuery()->execute();
    }

    /**
     * @param int $id
     * @return Event
     * @throws NoResultException
     */
    public function getEvent($id)
    {
        $qb = $this->createQueryBuilder('b');
        $qb->where('b.id = :id');
        $qb->setParameter('id', $id);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param float $lat
     * @param float $lng
     * @param int   $radius
     * @return Event[]
     */
    public function eventsInRadius($lat, $lng, $radius = 2)
    {
        # get metadata about Event entity
        $meta = $this->getEntityManager()
            ->getMetadataFactory()
            ->getMetadataFor(Event::class);

        # create SQL for finding proper events
        /** @noinspection UnknownInspectionInspection */
        /** @noinspection PhpUndefinedFieldInspection */
        $sql  = "SELECT
            *,
            (
                6371 * ACOS(
                    COS(RADIANS({$lat})) * COS(RADIANS(`latitude`)) * COS(RADIANS(`longitude`) 
                    - 
                    RADIANS({$lng})) + SIN(RADIANS({$lat})) * SIN(RADIANS(`latitude`))
                )
            ) AS distance
            FROM `".$meta->table['name']."`
            HAVING distance <= {$radius}
            ORDER BY distance ASC
        ";

        # send query
        $stmt = $this->getEntityManager()->getConnection()->query($sql);
        $stmt->execute();

        # get events and process associative array result to entities objects
        $events = $stmt->fetchAll();

        foreach($events as $i => $item) {
            $events[$i] = Event::fromArray($item);
        }

        # return events
        return $events;
    }
}