<?php

namespace LocalizationsBundle\Services;

/**
 * Class using Google Maps Geocode
 *
 * @package    LocalizationsBundle
 * @subpackage Services
 */
class Geocode
{
    /**
     * @param string $address
     * @return array
     */
    public function getCoordinates($address)
    {
        # create output variable
        $out = [];

        # check address
        if(is_string($address) && $address !== '') {
            # create URL to Geocode
            $url = 'https://maps.google.com/maps/api/geocode/json?address='
                .urlencode($address);

            # get response
            $response = file_get_contents($url);

            # get JSON with data
            $json = json_decode($response, true);

            # check if response is valid
            if($json['status'] === 'OK' && count($json['results']) > 0) {
                $lat  = $json['results'][0]['geometry']['location']['lat'];
                $long = $json['results'][0]['geometry']['location']['lng'];

                $out = [
                    'lat'  => $lat,
                    'long' => $long,
                ];
            }
        }

        # return proper value
        return $out;
    }
}