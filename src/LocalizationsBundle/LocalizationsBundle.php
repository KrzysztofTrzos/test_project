<?php

namespace LocalizationsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Main class for "Localizations" bundle.
 *
 * @package LocalizationsBundle
 */
class LocalizationsBundle extends Bundle
{
}
