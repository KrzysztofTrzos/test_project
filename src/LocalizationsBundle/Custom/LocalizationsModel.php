<?php

namespace LocalizationsBundle\Custom;

/**
 * This class is used to share some functionalities accros all entities.
 *
 * @package    LocalizationsBundle
 * @subpackage Custom
 */
abstract class LocalizationsModel
{
    /**
     * Making new instance of an entity, filling with proper values from
     * parameter.
     *
     * @param array $values
     * @return $this
     */
    public static function fromArray(array $values)
    {
        $class_name = static::class;
        $obj        = new $class_name;

        foreach($values as $key => $value) {
            $exploded = explode('_', $key);

            foreach($exploded as $i => $item) {
                $exploded[$i] = ucfirst($item);
            }

            $method = 'set'.implode($exploded);

            if(method_exists($class_name, $method)) {
                $obj->$method($value);
            }
        }

        return $obj;
    }

}