<?php

namespace LocalizationsBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class EventDaysLater
 *
 * @package    LocalizationsBundle
 * @subpackage Validator\Constraints
 */
class EventDateValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed      $value      The value that should be validated
     * @param Constraint|EventDate $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        $now       = new \DateTime();
        $sevenDays = new \DateTime($now->format('Y-m-d 00:00:00').' +7 days');

        if($sevenDays > $value) {
            $this->context->buildViolation($constraint->message, [])
                ->setParameter('%date%', $sevenDays->format('d.m.Y'))
                ->addViolation();
        }
    }
}