<?php

namespace LocalizationsBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class EventDaysLater
 *
 * @package    LocalizationsBundle
 * @subpackage Validator\Constraints
 * @Annotation
 */
class AddressGeocode extends Constraint
{
    /**
     * @var string
     */
    public $message = 'Given address is invalid.';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'address_geocode';
    }
}