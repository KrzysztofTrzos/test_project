<?php

namespace LocalizationsBundle\Validator\Constraints;

use LocalizationsBundle\Entity\Event;
use LocalizationsBundle\Services\Geocode;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class EventDaysLater
 *
 * @package    LocalizationsBundle
 * @subpackage Validator\Constraints
 */
class AddressGeocodeValidator extends ConstraintValidator
{
    /**
     * @var Geocode
     */
    private $geo;

    /**
     * Geocode constructor.
     *
     * @param Geocode $addressGeocode
     */
    public function __construct(Geocode $addressGeocode)
    {
        $this->geo = $addressGeocode;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param string                    $value      The value that should be
     *                                              validated
     * @param Constraint|AddressGeocode $constraint The constraint for the
     *                                              validation
     */
    public function validate($value, Constraint $constraint)
    {
        $coords = $this->geo->getCoordinates($value);

        if($coords === []) {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
        } else {
            /* @var $event Event */
            $event = $this->context->getObject();
            $event->setLatitude($coords['lat']);
            $event->setLongitude($coords['long']);
        }
    }
}