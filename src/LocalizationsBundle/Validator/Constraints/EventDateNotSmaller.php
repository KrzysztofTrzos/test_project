<?php

namespace LocalizationsBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class EventDaysLater
 *
 * @package    LocalizationsBundle
 * @subpackage Validator\Constraints
 * @Annotation
 */
class EventDateNotSmaller extends Constraint
{
    /**
     * @var string
     */
    public $message = 'This date can\'t be smaller than "Date From" field value.';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'event_date_not_smaller';
    }
}