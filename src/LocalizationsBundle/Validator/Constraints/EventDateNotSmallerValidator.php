<?php

namespace LocalizationsBundle\Validator\Constraints;

use LocalizationsBundle\Entity\Event;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class EventDaysLater
 *
 * @package    LocalizationsBundle
 * @subpackage Validator\Constraints
 */
class EventDateNotSmallerValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed      $value      The value that should be validated
     * @param Constraint|EventDate $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        /* @var $data Event */
        $data = $this->context->getRoot()->getData();

        if($data->getDateFrom() > $data->getDateTo()) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}