<?php

namespace LocalizationsBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class EventDaysLater
 *
 * @package    LocalizationsBundle
 * @subpackage Validator\Constraints
 * @Annotation
 */
class EventDate extends Constraint
{
    /**
     * @var string
     */
    public $message = 'Date defined in this field must be not earlier than %date%.';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'event_date';
    }
}