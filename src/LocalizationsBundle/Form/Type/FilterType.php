<?php
namespace LocalizationsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class NewsLocaleType
 *
 * @package    LocalizationsBundle
 * @subpackage Form\Type
 */
class FilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('address', Type\TextType::class, [
            'label' => 'Address',
        ]);

        # add submit button
        $builder->add('submit', Type\SubmitType::class, [
            'label' => 'filter'
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'class' => 'form-inline'
            ]
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'filter_events';
    }
}