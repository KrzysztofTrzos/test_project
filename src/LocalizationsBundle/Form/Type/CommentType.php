<?php

namespace LocalizationsBundle\Form\Type;

use LocalizationsBundle\Entity\EventComment;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

/**
 * Class CommentType
 *
 * @package    LocalizationsBundle
 * @subpackage Form\Type
 */
class CommentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('content', Type\TextareaType::class, [
            'label' => 'Content',
        ]);

        $builder->add('email', Type\TextType::class, [
            'label' => 'E-mail',
        ]);

        # add submit button
        $builder->add('submit', Type\SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EventComment::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'event_comment';
    }
}