<?php
namespace LocalizationsBundle\Form\Type;

use LocalizationsBundle\Entity\Event;
use LocalizationsBundle\Validator\Constraints;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

/**
 * Class NewsLocaleType
 *
 * @package    LocalizationsBundle
 * @subpackage Form\Type
 */
class EventType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        # create basic event fields
        $builder->add('name', Type\TextType::class, [
            'label' => 'Name',
        ]);

        $builder->add('description', Type\TextareaType::class, [
            'label' => 'Description',
        ]);

        $builder->add('address', Type\TextType::class, [
            'label' => 'Address',
        ]);

        $builder->add('email', Type\TextType::class, [
            'label' => 'E-mail',
        ]);

        $builder->add('date_from', Type\DateType::class, [
            'label'       => 'Date from',
            'constraints' => [
                new Constraints\EventDate(),
            ],
        ]);

        $builder->add('date_to', Type\DateType::class, [
            'label'       => 'Date to',
            'constraints' => [
                new Constraints\EventDateNotSmaller(),
            ],
        ]);

        # add submit button
        $builder->add('submit', Type\SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'     => Event::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'event_form';
    }


}