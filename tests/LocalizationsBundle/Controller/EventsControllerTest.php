<?php

namespace LocalizationsBundle\Controller;

use Doctrine\ORM\EntityManager;
use LocalizationsBundle\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest
 *
 * @package    LocalizationsBundle
 * @subpackage Tests\Controller
 */
class EventsControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Initialize all basic data.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->initClient();

        $kernel = static::createKernel();
        $kernel->boot();

        $this->em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->em->beginTransaction();
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
        $this->em = null;
    }

    /**
     * Prepare client and crawler for all tests in this class.
     */
    private function initClient()
    {
        $this->client = static::createClient();
    }

    /**
     * Test for "list" action (URL: /events).
     */
    public function testList()
    {
        # prepare crawler
        $crawler = $this->client->request('GET', '/events');

        # assertions
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(1, $crawler->filter('.events_list')->count());
        $this->assertEquals(
            $crawler->filter('.events_list h2.events_list__header')->count(),
            $crawler->filter('.events_list__link')->count()
        );

        $this->assertEquals(1, $crawler->filter('form[name=filter_events]')->count());
        $this->assertEquals(1, $crawler->filter('form[name=filter_events] input#filter_events_address')->count());
        $this->assertEquals(1, $crawler->filter('form[name=filter_events] button#filter_events_submit')->count());
    }

    public function testEventsJson()
    {

    }

    /**
     * Test for profile of an particular event (URL: /events/profile/{id}).
     */
    public function testProfile()
    {
        # prepare crawler for not existing event profile
        $this->client->request('GET', '/events/profile/9s0x099');

        # assertions
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());

        # prepare crawler for existing event profile
        $crawler = $this->client->request('GET', '/events/profile/1');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(1, $crawler->filter('.event_profile')->count());

        # check elements of an event
        $this->assertEquals(1, $crawler->filter('.event_profile__description')->count());
        $this->assertEquals(1, $crawler->filter('.event_profile__date_from')->count());
        $this->assertEquals(1, $crawler->filter('.event_profile__date_to')->count());
        $this->assertEquals(1, $crawler->filter('.event_profile__address')->count());

        # check h2 headers inside of event profile
        $this->assertEquals(4, $crawler->filter('.event_profile h2')->count());

        # check "go back" link
        $this->assertEquals(1, $crawler->filter('.goback')->count());
    }

    /**
     * Testing comments list in the event profile.
     */
    public function testCommentsList()
    {
        # prepare crawler for existing event profile
        $crawler = $this->client->request('GET', '/events/profile/1');

        # asserts
        $this->assertEquals(1, $crawler->filter('.comments_list')->count());
        $this->assertEquals(1, $crawler->filter('.comments_form')->count());
        $this->assertEquals(1, $crawler->filter('.comments_form__header')->count());
        $this->assertEquals(1, $crawler->filter('.comments_form form')->count());

        $this->assertEquals(1, $crawler->filter('.comments_form textarea#event_comment_content')->count());
        $this->assertEquals(1, $crawler->filter('.comments_form input#event_comment_email')->count());

        # load event
        $event = $this->em->find(Event::class, 1);

        # check if all comments are loaded
        $this->assertEquals(
            $event->getComments()->count(),
            $crawler->filter('.comments_list .comments_list__item')->count()
        );

        $this->assertEquals(
            $event->getComments()->count(),
            $crawler->filter('.comments_list .comments_list__delete')->count()
        );
    }

    /**
     * Test for "new" action (URL: /events/create).
     */
    public function testNew()
    {
        # prepare crawler
        $crawler = $this->client->request('GET', '/events/create');

        # basic assertions
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(1, $crawler->filter('form[name=event_form]')->count());
        $this->assertEquals(1, $crawler->filter('form[name=event_form] #event_form_name')->count());
        $this->assertEquals(1, $crawler->filter('form[name=event_form] #event_form_description')->count());
        $this->assertEquals(1, $crawler->filter('form[name=event_form] #event_form_address')->count());
        $this->assertEquals(1, $crawler->filter('form[name=event_form] #event_form_email')->count());
        $this->assertEquals(1, $crawler->filter('form[name=event_form] #event_form_date_from')->count());
        $this->assertEquals(1, $crawler->filter('form[name=event_form] #event_form_date_to')->count());
    }

    /**
     * @dataProvider dataProviderForEventForm
     *
     * @param array    $data
     * @param callable $assertions
     * @param int      $helpBlocksAmount
     */
    public function testEventForm(array $data, callable $assertions, $helpBlocksAmount)
    {
        # prepare crawler
        $crawler = $this->client->request('GET', '/events/create');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        # form sending assert
        $form = $crawler->filter('#event_form_submit')->form();

        # submit form
        $this->client->submit($form, $data);

        # some variables
        $content = $this->client->getResponse()->getContent();
        $crawler = $this->client->getCrawler();

        # some preliminary assertions
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($crawler->filter('.help-block')->count(), $helpBlocksAmount);

        # assertions
        $assertions($this, $content);
    }

    /**
     * @return array
     */
    public function dataProviderForEventForm()
    {
        return [
            'scenario_1' => [
                [
                    'event_form[name]' => '',
                ],
                function (KernelTestCase $testCase, $content) {
                    $testCase->assertContains('This value should not be blank', $content);
                    $testCase->assertContains('Given address is invalid', $content);
                    $testCase->assertContains('Date defined in this field must be not earlier than', $content);
                },
                5,
            ],
            'scenario_2' => [
                [
                    'event_form[name]'        => ''
                        .'sds daasd das adsdas sasad saadas adsads s ds das ads'
                        .'ads das dadas dasadssdsaads sad  adsads adsadsasd ada'
                        .'sdassadadsads adssadads',
                    'event_form[email]'       => 'test',
                    'event_form[description]' => 'test2',
                ],
                function (KernelTestCase $testCase, $content) {
                    $testCase->assertContains('This value should not be blank', $content);
                    $testCase->assertContains('Given address is invalid', $content);
                    $testCase->assertContains('This value is too long. It should have 50 characters or less', $content);
                    $testCase->assertContains('This value is not a valid email address', $content);
                },
                4,
            ],
        ];
    }

    /**
     * @dataProvider dataProviderForCommentsForm
     *
     * @param array    $data
     * @param callable $assertions
     * @param int      $helpBlocks
     */
    public function testCommentForm(array $data, callable $assertions, $helpBlocks)
    {
        # prepare crawler
        $crawler = $this->client->request('GET', '/events/profile/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        # form sending assert
        $form = $crawler->filter('#event_comment_submit')->form();

        # submit form
        $this->client->submit($form, $data);

        # some variables
        $content = $this->client->getResponse()->getContent();
        $crawler = $this->client->getCrawler();

        # some preliminary assertions
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($crawler->filter('.help-block')->count(), $helpBlocks);

        # assertions
        $assertions($this, $content);
    }

    /**
     * @return array
     */
    public function dataProviderForCommentsForm()
    {
        return [
            'scenario_1_comments' => [
                [
                    'event_comment[content]' => '',
                    'event_comment[email]'   => '',
                ],
                function (KernelTestCase $testCase, $content) {
                    $testCase->assertContains('This value should not be blank', $content);
                },
                2,
            ],
            'scenario_2_comments' => [
                [
                    'event_comment[content]' => 'test',
                    'event_comment[email]'   => 'test2',
                ],
                function (KernelTestCase $testCase, $content) {
                    $testCase->assertContains('This value is not a valid email address', $content);
                },
                1,
            ],
        ];
    }
}
