<?php

namespace LocalizationsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class CommentsControllerTest
 *
 * @package    LocalizationsBundle
 * @subpackage Controller
 */
class CommentsControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * Initialize all basic data.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->initClient();
    }

    /**
     * Prepare client and crawler for all tests in this class.
     */
    private function initClient()
    {
        $this->client = static::createClient();
    }

    /**
     *
     */
    public function testCommentDelete()
    {
        # prepare crawler for not existing comment
        $this->client->request('GET', '/events/comment/delete/9s0x099');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());

        # prepare crawler for existing event profile
        $this->client->request('GET', '/events/comment/delete/1');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
    }

}
