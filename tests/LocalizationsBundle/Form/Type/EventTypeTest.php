<?php

namespace LocalizationsBundle\Form\Type;

use LocalizationsBundle\Entity\Event;
use Symfony\Component\Form\Extension\Core\CoreExtension;
use Symfony\Component\Form\Extension\Validator\Type\FormTypeValidatorExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class EventTypeTest
 *
 * @package    LocalizationsBundle
 * @subpackage Form\Type
 */
class EventTypeTest extends TypeTestCase
{
    /**
     * @var CommentType|FormInterface
     */
    private $form;

    /**
     * First operations, before making tests.
     */
    protected function setUp()
    {
        parent::setUp();

        # this code beneath prevents from test not seeing any constraints
        /* @var $validator ValidatorInterface|\PHPUnit_Framework_MockObject_MockObject */

        $validator = $this->createMock(ValidatorInterface::class);
        $validator->method('validate')->will($this->returnValue(new ConstraintViolationList()));

        $formTypeExtension = new FormTypeValidatorExtension($validator);
        $coreExtension     = new CoreExtension();

        $this->factory = Forms::createFormFactoryBuilder()
            ->addExtensions($this->getExtensions())
            ->addExtension($coreExtension)
            ->addTypeExtension($formTypeExtension)
            ->getFormFactory();

        # create form
        $this->form = $this->factory->create(EventType::class);
    }

    /**
     * Checking if particular form type has proper fields.
     */
    public function testHasFields()
    {
        $this->assertNotNull(
            $this->form->get('name')
        );

        $this->assertNotNull(
            $this->form->get('description')
        );

        $this->assertNotNull(
            $this->form->get('email')
        );

        $this->assertNotNull(
            $this->form->get('address')
        );

        $this->assertNotNull(
            $this->form->get('date_from')
        );

        $this->assertNotNull(
            $this->form->get('date_to')
        );
    }

    /**
     *
     */
    public function testSubmitValidData()
    {
        $formData = [
            'date_from'   => [
                'year'  => '2016',
                'month' => '1',
                'day'   => '1',
            ],
            'date_to'     => [
                'year'  => '2016',
                'month' => '1',
                'day'   => '1',
            ],
            'name'        => 'test2',
            'description' => 'description',
            'address'     => 'x',
            'email'       => 'test@domain.com',
        ];

        $object = Event::fromArray($formData);

        // submit the data to the form directly
        $this->form->submit($formData);

        $this->assertTrue($this->form->isSynchronized());
        $this->assertEquals($object, $this->form->getData());

        $view     = $this->form->createView();
        $children = $view->children;

        foreach(array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

    /**
     *
     */
    public function testSubmitNotValidData()
    {
        # initalize form data
        $data = [
            'date_from'   => [
                'year'  => '2016',
                'month' => '1',
                'day'   => '1',
            ],
            'date_to'     => [],
            'name'        => '',
            'description' => 'description',
            'address'     => 'x',
            'email'       => '',
        ];

        # create form and submit the data to the form directly
        $this->form->submit($data);

        # objects
        /* @var $formData Event */
        $object   = Event::fromArray($data);
        $formData = $this->form->getData();

        # asserts
        $this->assertTrue($this->form->isSynchronized());
        $this->assertNotSame($object->getName(), $formData->getName());
        $this->assertNotSame($object->getDateTo(), $formData->getDateTo());
        $this->assertNotSame(gettype($object->getEmail()), gettype($formData->getEmail()));
    }

//    public function testCharsLimit()
//    {
//        # initialize form data
//        $data = [
//            'date_from'   => '',
//            'date_to'     => '',
//            'name'        => '',
//            'description' => '',
//            'address'     => '',
//            'email'       => 'addssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsadsaddssadadsdasaddassdadssadadsdassaddsasddasadsads',
//            'latituddasdsadsasdsaddassade'    => '',
//            'longitude'   => '',
//        ];
//
//        # submit form
//        $this->form->submit($data);
//
//        # check errors
//        dump($this->form->isSubmitted(), $this->form->getData(),$this->form->isValid());
//    }

//    public function testCheckRequired()
//    {
//        # initialize form data
//        $data = [
//            'date_from'   => '',
//            'date_to'     => '',
//            'name'        => null,
//            'description' => '',
//            'address'     => '',
//            'email'       => '',
//            'latitude'    => '',
//            'longitude'   => '',
//        ];
//
//        # create form and submit the data to the form directly
//        $form = $this->factory->create(EventType::class);
//        $form->submit($data);
//
//        dump($form->isSubmitted(), $form->isValid(), $form->isSynchronized());
//    }
}