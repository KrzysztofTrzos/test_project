<?php

namespace LocalizationsBundle\Form\Type;

use LocalizationsBundle\Entity\EventComment;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Test\TypeTestCase;

/**
 * Test class for EventComment entity form.
 *
 * @package    LocalizationsBundle
 * @subpackage Form\Type
 */
class CommentTypeTest extends TypeTestCase
{
    /**
     * @var CommentType|FormInterface
     */
    private $form;

    /**
     * First operations, before making tests.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->form = $this->factory->create(CommentType::class);
    }

    /**
     * Checking if particular form type has proper fields.
     */
    public function testHasFields()
    {
        $this->assertNotNull(
            $this->form->get('email')
        );
        $this->assertNotNull(
            $this->form->get('content')
        );
    }

    /**
     * Test data validation.
     */
    public function testSubmitValidData()
    {
        $formData = [
            'content' => 'content',
            'email'   => 'test@domain.com',
        ];

        $object = EventComment::fromArray($formData);

        // submit the data to the form directly
        $this->form->submit($formData);

        $this->assertTrue($this->form->isSynchronized());
        $this->assertEquals($object, $this->form->getData());

        $view     = $this->form->createView();
        $children = $view->children;

        foreach(array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}
