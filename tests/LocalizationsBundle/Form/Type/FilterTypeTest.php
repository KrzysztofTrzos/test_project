<?php

namespace LocalizationsBundle\Form\Type;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Test\TypeTestCase;

/**
 * Test class for EventComment entity form.
 *
 * @package    LocalizationsBundle
 * @subpackage Form\Type
 */
class FilterTypeTest extends TypeTestCase
{
    /**
     * @var CommentType|FormInterface
     */
    private $form;

    /**
     * First operations, before making tests.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->form = $this->factory->create(FilterType::class);
    }

    /**
     * Checking if particular form type has proper fields.
     */
    public function testHasFields()
    {
        $this->assertNotNull(
            $this->form->get('address')
        );
    }
}
