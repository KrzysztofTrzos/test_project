<?php

namespace LocalizationsBundle\Custom;

use LocalizationsBundle\Entity\Event;
use LocalizationsBundle\Entity\EventComment;

/**
 * Tests for LocalizationsModel class.
 *
 * @package    LocalizationsBundle
 * @subpackage Custom
 */
class LocalizationsModelTest extends \PHPUnit_Framework_TestCase
{
    public function testMain()
    {
        $class = new \ReflectionClass(LocalizationsModel::class);

        $this->assertTrue(
            $class->isAbstract()
        );
    }

    public function testValues()
    {
        $this->assertTrue(is_subclass_of(Event::class, LocalizationsModel::class));
        $this->assertTrue(is_subclass_of(EventComment::class, LocalizationsModel::class));

        $data = [
            'name'        => 'xx',
            'description' => 'yy',
            'xyz'         => 'zyx',
        ];

        $event = Event::fromArray($data);

        $this->assertEquals($event->getName(), $data['name']);
        $this->assertEquals($event->getDescription(), $data['description']);
    }
}
