<?php

namespace LocalizationsBundle\Services;

/**
 * This class is testing the Geocode class.
 *
 * @package    LocalizationsBundle
 * @subpackage Services
 */
class GeocodeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Geocode
     */
    private $geocode;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->geocode = new Geocode();
    }


    /**
     * @dataProvider formDataToTest
     * @param string $addr
     * @param array $data
     */
    public function testCoordinatesLoad($addr, array $data)
    {
        $this->assertEquals(
            $this->geocode->getCoordinates($addr),
            $data
        );
    }

    /**
     * Data provider for testCoordinatesLoad.
     *
     * @return array
     */
    public function formDataToTest()
    {
        return [
            'scenario_1' => [
                'xyz111000222',
                [],
            ],
            'scenario_2' => [
                'Miodowa 1, Warszawa, Poland',
                [
                    'lat'  => 52.245976,
                    'long' => 21.0131999,
                ],
            ],
            'scenario_3' => [
                '',
                [],
            ],
        ];
    }
}
