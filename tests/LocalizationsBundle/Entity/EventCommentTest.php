<?php

namespace LocalizationsBundle\Entity;

use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class EventTest
 *
 * @package    LocalizationsBundle
 * @subpackage Tests\Entity
 */
class EventCommentTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null;
    }

    public function testCreation()
    {
        # create new comment
        $comment = new EventComment();

        # check if comment creation date is not null
        $this->assertNotNull(
            $comment->getCreationDate()
        );
    }

    /**
     * Check if comment without event can be pushed to database.
     */
    public function testCommentWithoutEvent()
    {
        # create new comment without related event in it
        $comment = new EventComment();
        $comment->setContent('test content');
        $comment->setEmail('test e-mail');

        # try to persist and flush
        try {
            $this->em->persist($comment);
            $this->em->flush();
        } catch(NotNullConstraintViolationException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail();
    }
}
